(ns homebridge-artnet.animation)

;; Config
(def animation-duration 1000)

; State
(def animations (atom {}))


;; Animation

(defn transition-pow
  ; from https://gist.github.com/raspasov/f9ca712571efd932169e
  "Calculates a power transition.
  'x' the the power to raise to.
  'pos' is a number between 0 and 1 representing the position of the transition between
  starting (0) and finished (1)"
  (^double [^double pos] (transition-pow 2 pos))
  (^double [^double x ^double pos] (js/Math.pow pos x)))

(defn ease-in-out
  ; from https://gist.github.com/raspasov/f9ca712571efd932169e
  "Eases in to then out of of a transition where
  'transition' is a function defining the transition type and
  'p' is a number between 0 and 1 representing the position of the transition between
  starting (0) and finished (1)"
  ^double
  [transition ^double p]
  (if (<= p (double 0.5))
    (/ (transition (* (double 2) p))
       (double 2))
    (/ (- 2 (transition (* (double 2) (- (double 1) p))))
       (double 2))))



(defn add-animation!
  "docstring"
  ([channel to-value]
   (add-animation! {channel to-value}))
  ([data]
   (let [start-time (.getTime (js/Date.))]
     (doseq [[channel to-value] data]
       (swap! animations assoc channel {:to    to-value
                                        :start start-time})))))

(defn set-channel!
  "docstring"
  [state channel val]
  (swap! state assoc (- channel 1) (.round js/Math val)))

(defn process-animations!
  "docstring"
  [dmx-state]
  (when (pos? (count @animations))
    (let [current-time (.getTime (js/Date.))]
      (doseq [[channel data] @animations]
        (let [from (or (:from data) (nth @dmx-state (- channel 1)))]

          ; setting from if not present
          (when-not (:from data)
            (->> (assoc data :from from) (swap! animations assoc channel)))

          ; Check if animation is already over
          (if (>= current-time (+ (:start data) animation-duration))
            (do
              (swap! animations dissoc channel)
              (set-channel! dmx-state channel (:to data)))
            (let [progress-lin (/ (- current-time (:start data)) animation-duration)
                  ;progress     (ease-in-out transition-pow progress-lin)
                  progress     (ease-in-out transition-pow progress-lin)
                  step         (-> (:to data) (- from) (* progress))]
              (set-channel! dmx-state channel (+ from step)))))))))